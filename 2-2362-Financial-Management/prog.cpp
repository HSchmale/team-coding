#include <iostream>
#include <cstdio>

using std::cin;


int main() {
    double d, tot = 0;
    size_t n = 0;
    while(cin >> d && ++n)
        tot += d;
    printf("$%.2lf\n", tot / n);
    return 0;
}
