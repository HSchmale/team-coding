/**
 * Fall 2017 Packet 1 - 10115 Automatic Editing
 * @author Henry J Schmale
 * @date 09-14-2017
 */

#include <string>
#include <algorithm>
#include <cstdio>
#include <vector>
#include <iostream>

using std::string;
using std::vector;
using std::cin;
using std::cout;
using std::pair;
using std::make_pair;
using std::getline;

string applyTransformation(string str, string toFind, string toReplace) {
    size_t pos = str.find(toFind);
    // return the default if not found
    if(pos == string::npos)
        return str;
    str.replace(pos, toFind.length(), toReplace);
    return applyTransformation(str, toFind, toReplace);
}



int main(int argc, char** argv) {
    int count = 0;
    
    while(cin >> count && count != 0) {
        string IGNORE_REMAINDER_OF_COUNT;
        getline(cin, IGNORE_REMAINDER_OF_COUNT);

        vector<pair<string,string>> rules;
        for(int i = 0; i < count; ++i) {
            string toFind, toReplace;
            getline(cin, toFind);
            getline(cin, toReplace);
            rules.push_back(make_pair(toFind, toReplace));
        }

        string edit;
        getline(cin, edit);

        for(pair<string,string>& p : rules) {
            edit = applyTransformation(edit, p.first, p.second);
        }
        printf("%s\n", edit.c_str());
    }

    return 0;    
}
