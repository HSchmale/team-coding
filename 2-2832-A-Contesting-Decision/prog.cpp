#include <iostream>
#include <cstdio>
#include <sstream>
#include <algorithm>
#include <vector>

using std::stringstream;
using std::string;
using std::cin;
using std::cout;
using std::vector;
using std::endl;

struct probAttempt {
    unsigned cnt;
    unsigned time;
};

struct teamLine {
    string m_name;
    vector<probAttempt> m_attempts;
    unsigned m_points;
    unsigned m_solved;

    teamLine(string name, vector<probAttempt> attempts) 
            : m_name(name), m_attempts(attempts), m_points(0), m_solved(0) {
        for(const auto& atmpt : m_attempts) {
            if(atmpt.time == 0) 
                continue;
            ++m_solved;
            m_points += atmpt.time + atmpt.cnt * 20;
        }
    }

    void print(std::ostream& out) {
        out << m_name << " " << m_solved << " " << m_points << endl;
    }
};

probAttempt readAttempt(stringstream& sstr) {
    unsigned attempts, time;
    sstr >> attempts;
    sstr >> time;
    return {attempts, time};
}

teamLine readLine(string line) {
    string name;
    vector<probAttempt> data;
    stringstream sstr(line);

    sstr >> name;
    while(sstr.peek() != EOF)
        data.push_back(readAttempt(sstr));

    return teamLine(name, data);
}

bool compareTeamProbs(const teamLine& a, const teamLine& b) {
    if(a.m_solved >= b.m_solved) {
        if(a.m_solved == b.m_solved)
            return a.m_points < b.m_points;
        else return true;
    }
    return false;   
}

int main() {
    int numTeams;
    cin >> numTeams;
    vector<teamLine> teams;

    string line;
    while(getline(cin, line)) {
        // stupid readline thing, cin doesn't consume the new lines
        if(line.empty())
            continue;
        teams.push_back(readLine(line));
    }
    
    sort(teams.begin(), teams.end(), compareTeamProbs);
    teams[0].print(cout);

    return 0;
}
