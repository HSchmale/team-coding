import sys



graph = []
n_water, m_trails = map(int, input().split())
for line in sys.stdin.readlines():
    w1, w2, length = map(int, input().split())
    graph.append((w1, w2, length)) 
