import string

s = input()
tot = len(s)

w = [x for x in s if x == '_']
l = [x for x in s if x in string.ascii_lowercase]
u = [x for x in s if x in string.ascii_uppercase]
sy = tot - (len(w) + len(l) + len(u))

print(len(w)/tot)
print(len(l)/tot)
print(len(u)/tot)
print(sy / tot)
