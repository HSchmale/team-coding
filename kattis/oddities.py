n = int(input())
for i in range(n):
    x = int(input())
    print('{} is {}'.format(x, "even" if x % 2 == 0 else "odd"))
