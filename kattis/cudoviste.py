#!/usr/bin/python2.7

from sys import stdin
from collections import OrderedDict

BUILDING = '#'
CAR = 'X'
FREE = '.'

def get2by2(ar2d, x, y):
    return ar2d[y][x:x+2] + ar2d[y+1][x:x+2]

rc = raw_input().split()
rows = int(rc[0])
cols = int(rc[1])

space = [];
for line in stdin.readlines():
    space.append(line.strip());

parking = OrderedDict({0:0, 1:0, 2:0, 3:0, 4:0})
for col in range(cols - 1):
    for row in range(rows - 1):
        park = get2by2(space, col, row)
        if BUILDING not in park:
            parking[park.count(CAR)] += 1

for k,v in parking.items():
    print(v)
