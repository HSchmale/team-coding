from functools import reduce

def line_ints():
    return list(map(int, input().split()))

while True:
    n = int(input())
    if n == -1: break
    m = reduce(lambda a, x: a + x[0] * x[1], 
                (line_ints() for x in range(n)), 0)
    print(str(m) + " miles") 
