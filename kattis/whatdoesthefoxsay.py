cases = int(input())

def test_case():
    recording = input().split()
    inline = input().split()
    words = []
    while len(inline) != 5:
        words.append(inline[-1])
        inline = input().split()
    fox = [x for x in recording if x not in words]
    print(' '.join(fox))

for i in range(cases):
    test_case()
