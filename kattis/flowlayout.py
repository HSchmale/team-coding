def test_case():
    max_width = int(input())
    boxes = []
    while True:
        w, h = map(int, input().split())
        if w == -1 or h == -1:
            break
        boxes.append((w,h))
    tot_max_width = 0
    tot_max_height = 0
    cur_row_pos = 0
    cur_height = 0
    for w,h in boxes:
        if cur_row_pos + w > max_width:
           tot_max_height += cur_height
           cur_height = 0
           cur_row_pos = 0
        if h > cur_height:
            cur_height = h
        cur_row_pos += w
        if cur_row_pos > tot_max_width:
            tot_max_width = cur_row_pos
    tot_max_height += cur_height
    print("{} x {}".format(tot_max_width, tot_max_height))

while True:
    try:
        test_case()
    except:
        break
