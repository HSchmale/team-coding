#include <vector>
#include <set>
#include <iostream>
#include <math.h>
#include <bitset>
#include <numeric>

using std::set;
using std::vector;
using std::cin;
using std::cout;

std::vector<bool>
sieveVector (unsigned N)
{
    std::vector<bool> vec(N + 1, true);
    set<unsigned> s;
    for (unsigned i = 2; i <= sqrt(N); ++i)
        if (vec[i])
            for (unsigned j = i * i; j <= N; j += i)
                vec[j] = false;
    return vec;
}

int main() {
    unsigned n, q, x;

    cin >> n >> q; 

    std::vector<bool> primes = sieveVector(n + 1);
    primes[0] = false;
    primes[1] = false;

    cout << std::accumulate(primes.begin(), primes.end(), 0) << "\n";
    for(int count = 0; count < q; ++count) {
        cin >> x;
        cout << primes[x] << "\n";
    }
    return 0;
}
