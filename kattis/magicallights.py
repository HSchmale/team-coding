from collections import Counter

# This problem is one indexed
# I did rebasing to get it to be zero indexed

class MagicalTree:
    def __init__(self, colors, parents):
        self.parents = [None]
        self.parents.extend(x - 1 for x in parents)
        
        self.colors = colors
        self.children = [[]]*len(colors)
        #self.children = [[] for i in range(len(colors))]
        
        # child - index of node within parents which has N - 1 things
        #           in it
        # parent - index of the parent node of child
        iterparents = enumerate(self.parents)
        next(iterparents) 
        for child, parent in iterparents: 
            self.children[parent].append(child)
        
        self.child_cache = [Counter() for i in range(len(colors))]
        self.create_cache_table(0)
        
    def create_cache_table(self, x):
        for child in self.children[x]: 
            self.create_cache_table(child)
            self.child_cache[x] += self.child_cache[child]
        self.child_cache[x][self.colors[x]] += 1

    def update_node(self, x, k):
        old_color = self.colors[x]
        self.colors[x] = k

        node = x 
        while self.parents[node] is not None:
            self.child_cache[node][old_color] -= 1
            self.child_cache[node][k] += 1
            node = self.parents[node]

        # update 0th node
        self.child_cache[0][old_color] -= 1
        self.child_cache[0][k] += 1
 
    def query_magic(self, x):
        return sum(1 for i in self.child_cache[x].values() if i & 1)

def main():
    N, Q = map(int, input().split())
    colors = [int(x) for x in input().split()]
    parents = [int(x) for x in input().split()]
    tree = MagicalTree(colors, parents)
    for i in range(Q):
        k, x = map(int, input().split())
        if k == 0:
            print(tree.query_magic(x - 1))
        else:
            tree.update_node(x - 1, k)


if __name__ == '__main__':
    main() 
