import datetime

d, m = map(int, input().split())

date = datetime.date(2009, m, d)
print(date.strftime("%A"))
