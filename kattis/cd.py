#!/usr/bin/env python2.7

from sets import Set

def do_test():
    jack = Set()
    jill = Set()

    cases = map(int, raw_input().split())
    if cases[0] == 0 and cases[1] == 0:
        return

    for i in range(cases[0]):
        jack.add(int(raw_input()))

    for i in range(cases[1]):
        jill.add(int(raw_input()))

    x = jack & jill

    print(len(x))

while True:
    try:
        do_test()
    except EOFError:
        break
