complete = [1, 1, 2, 2, 2, 8]
current = [int(x) for x in input().split()]
print(' '.join([str(com - cur) for cur, com in zip(current, complete)]))
