x = input()
y = input()

if len(x) < len(y):
    x = x.zfill(len(y))
elif len(y) < len(x):
    y = y.zfill(len(x))

# x, y
s1, s2 = "", ""

for i,j in zip(x,y):
    if i < j:
        s1 += j
    elif j < i:
        s2 += i
    else:
        s1 += i
        s2 += j

print("YODA" if len(s2) == 0 else int(s2))
print("YODA" if len(s1) == 0 else int(s1))

