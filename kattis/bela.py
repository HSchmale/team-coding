import sys

card_values = {
    'A': (11,11),
    'K': (4,4),
    'Q': (3,3),
    'J': (20, 2),
    'T': (10, 10),
    '9': (14, 0),
    '8': (0, 0),
    '7': (0,0)
}

hands, suit = input().split()
hands = int(hands)
accum = 0
for _ in range(hands*4):
    x = input()
    if x[1] == suit:
        y = 0
    else:
        y = 1
    z = card_values[x[0]][y]
    accum += z
print(accum)
