import sys
for line in sys.stdin.readlines():
    A, B = map(int, line.split())
    print(max(A, B) - min(A, B))
