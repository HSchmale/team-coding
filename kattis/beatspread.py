cases = int(input())
for i in range(cases):
    ssum, delta = map(int, input().split())
    if delta > ssum:
        print("impossible")
        continue
    if ssum % 2 == 1:
        if delta == 0:
            print('impossible')
            continue
        x, y = delta + 1, ssum % 2
    else:
        x, y = delta, 0
    while x + y < ssum:
        x += 1
        y += 1
    if x + y == ssum:
        print(x, y)
    else:
        print('impossible')
