import math

a, n = map(float, raw_input().split())

r = math.sqrt(a / math.pi)
p = 2 * r * math.pi

if p <= n:
    print "Diablo is happy!"
else:
    print "Need more materials!"
