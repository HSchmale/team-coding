import sys

factor_dict = {}
def prime_factors(n):
    if n in factor_dict:
        return factor_dict[n]
    old_n = n
    i = 2
    factors = 0
    while i * i <= n:
        if n % i:
            i += 1
        else: 
            n //= i
            factors += i
    if n > 1:
        factors += n
    factor_dict[old_n] = factors
    return factors


def prime_reduction(x):
    count = 1 
    while not x in primes:
        x = prime_factors(x)
        count += 1
    return y, count
    
for x in sys.stdin.readlines():
    print('{} {}'.format(*prime_reduction(int(x))))
