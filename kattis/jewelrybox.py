import math

def quad_form(a, b, c):
    bot = 2 * a
    rt = math.sqrt(b ** 2 - 4 * a * c)
    return ((-b + rt) / bot, (-b - rt) / bot)

def select_in_range(t, l, u):
    for i in t:
        if l < i < u:
            return i
    raise ValueError

# V(h) = h * (x-2h) * (y-2h)
# V(h) = 4h**3 - 2Xh**2 - 2Yh**2 + hXY
# V'(h) = 12h**2 - 4Xh - 4Yh + XY
def optimize_box(x, y):
    def vol(h, x, y):
        return h * (x - 2 * h) * (y - 2 * h)
    a = 12
    b = (-4 * x) + (-4 * y)
    c = x * y
    r1, r2 = quad_form(a, b, c)
    res = (vol(r1, x, y), vol(r2, x, y))
    return max(res)

def main():
    t = int(input())
    for i in range(t):
        x, y = map(int, input().split())
        print(optimize_box(x, y))



if __name__ == '__main__':
    main()
