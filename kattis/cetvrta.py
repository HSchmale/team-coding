import sys

x = []
y = []
points = set()


for _ in range(3):
    x0, y0 = map(int, input().split())
    x.append(x0)
    y.append(y0)
    points.add((x0, y0))

for x0 in x:
    for y0 in y:
        if (x0, y0) not in points:
            print(x0, y0)
            sys.exit(0)
            

