

def case(s):
    num, source, target = s.split()
    src_base = len(source)
    tar_base = len(target)
    

    value = 0
    for p, val in enumerate(reversed(num)):
        value += source.index(val) * (src_base ** p)

    stack = []
    p = tar_base
    while value != 0:
        digit = value % p
        value = int(value / p)
        stack.append(target[digit]) 

    return ''.join(reversed(stack))


n = int(input())
for i in range(1, n + 1):
    print("Case #" + str(i) + ":", case(input()))
