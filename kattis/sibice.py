import math
n, w, h = map(int, input().split())
m = math.sqrt(w**2 + h**2) 
for i in range(n):
    x = int(input())
    if x <= m: print("DA")
    else: print("NE")
