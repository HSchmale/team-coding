import java.util.Scanner;
import java.util.Stack;
import java.util.ArrayList;
import java.util.List;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.BufferedOutputStream;
import java.util.regex.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class stringmatching {
    static private ArrayList<Integer> foundPos = new ArrayList<>(200);
    static BufferedReader scanner = new BufferedReader(new InputStreamReader(System.in));
    static OutputStream out = new BufferedOutputStream(System.out);

    public static void old_attempt(String pattern, String line) throws IOException {
        foundPos.clear();

        if (pattern.length() > line.length()) {
            out.write('\n');
            return;
        }
        /*
        nextTry:
        for (int lineIndex = 0; lineIndex < line.length(); ++lineIndex) {
            for (int patternIndex = 0; patternIndex < pattern.length(); ++patternIndex) {
                int lineCheckLoc = lineIndex + patternIndex;
                if (lineCheckLoc >= line.length())
                    break nextTry;
                if (line.charAt(lineCheckLoc) != pattern.charAt(patternIndex)) {
                    continue nextTry;
                }
            }
            pos.add(lineIndex);
        }
        */
        Matcher matcher = Pattern.compile(pattern).matcher(line);

        while (matcher.find()) {
            foundPos.add(matcher.start());
        }

    }

    static void printResults() {
        try {
            if (foundPos.size() > 0) {
                out.write(foundPos.get(0).toString().getBytes());
                for (int i = 1; i < foundPos.size(); ++i) {
                    out.write(32);
                    out.write(foundPos.get(i).toString().getBytes());
                }
            }
            out.write('\n');
        } catch (IOException e) {
        }
    }

    public static int[] kmp_table(final String word) {
        int pos = 1;
        int cnd = 0;
        int[] table = new int[word.length() + 1];

        table[0] = -1;

        while (pos < word.length()) {
            if (word.charAt(pos) == word.charAt(cnd)) {
                table[pos] = table[cnd];
                ++pos;
                ++cnd;
            } else {
                table[pos] = cnd;
                cnd = table[cnd];
                while (cnd >= 0 && word.charAt(pos) != word.charAt(cnd)) {
                    cnd = table[cnd];
                }
                ++pos;
                ++cnd;
            }
        }
        table[pos] = cnd;

        return table;
    }

    public static void kmp(final String word, final String line) {
        foundPos.clear();
        int lineMatch = 0;
        int wordMatch = 0;
        final int[] table = kmp_table(word);

        while (lineMatch + wordMatch < line.length()) {
            if (word.charAt(wordMatch) == line.charAt(lineMatch + wordMatch)) {
                if ((wordMatch + 1) == word.length()) {
                    foundPos.add(lineMatch);
                    lineMatch = lineMatch + wordMatch - table[wordMatch];
                    wordMatch = table[wordMatch];
                } else {
                    ++wordMatch;
                }
            } else {
                // No Match At Char
                if (table[wordMatch] > -1) {
                    lineMatch = wordMatch + lineMatch - table[wordMatch];
                    wordMatch = table[wordMatch];
                } else {
                    lineMatch = lineMatch + wordMatch + 1;
                    wordMatch = 0;
                }
            }
        }
        printResults();
    }

    public static void printTable(int[] table) {
        for (Integer i : table) {
            System.out.print(i + " ");
        }
        System.out.println();
    }

    public static void main(String[] args) {
        try {
            String pattern = scanner.readLine();
            String line = scanner.readLine();
            while (line != null && pattern != null) {
                //do_test(pattern, line);
                kmp(pattern, line);
                pattern = scanner.readLine();
                line = scanner.readLine();
            }
            out.flush();
        } catch (IOException e) {
            // Something unrecovereable happened
        }
    }
}
