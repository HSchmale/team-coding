def make_report(n):
    d = {}
    for i in range(n):
        l = input().split()
        for food in l[1:]:
            try:
                d[food].append(l[0]) 
            except KeyError:
                d[food] = [l[0]]
    for food, peeps in sorted(d.items()):
        print(food, ' '.join(sorted(peeps)))
    print()

n = int(input())
while n != 0:
    make_report(n)
    n = int(input())

