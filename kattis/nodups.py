import sys
x = input()
y = set()

for w in x.split():
    if w in y:
        print('no')
        sys.exit(0)
    y.add(w)
print('yes')
