from itertools import permutations

arr = [int(x) for x in input().split()]
t2 = arr[-1]
t1 = arr[-2]
boxes = arr[:6]

for arrange in permutations(boxes):
    a = sum(arrange[:3])
    b = sum(arrange[3:])
    if a == t1 and b == t2:
        arrange = sorted(arrange[:3], reverse=True) + sorted(arrange[3:], reverse=True)
        print(' '.join(str(x) for x in arrange))
        break
