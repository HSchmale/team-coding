# 2048.py
# Solution to the 2048 problem on kattis
# Henry J Schmale
# May 16, 2018

def load_grid():
    return [
        [int(x) for x in input().split()],
        [int(x) for x in input().split()],
        [int(x) for x in input().split()],
        [int(x) for x in input().split()]
    ]

def print_grid(grid):
    for row in grid:
        print(*row)

def do_row(row):
    merged = False
    row = [c for c in row if c != 0]
    if len(row) == 0:
        return [0 for _ in range(4)]
    newrow = [row[0]]
    for c in row[1:]:
        if c == newrow[-1] and not merged:
            newrow[-1] += c
            merged = True
        else:
            newrow.append(c)
            merged = False
    remain = 4 - len(newrow)
    newrow.extend(0 for _ in range(remain))
    return newrow

def swipe_horizontal(grid, left=True):
    if left: 
        return [do_row(row) for row in grid]
    else:
        return [list(reversed(do_row(list(reversed(row))))) for row in grid]

def row_2_col(grid):
    grid2 = [[] for _ in range(4)]
    for row in grid:
        for i, c in enumerate(row):
            grid2[i].append(c)
    return grid2 

def swipe_vertical(grid, up=True):
    grid = row_2_col(grid)
    grid = swipe_horizontal(grid, up)
    grid = row_2_col(grid)
    return grid


def main():
    grid = load_grid()
    move = int(input())
    if move & 1 == 1:
        grid = swipe_vertical(grid, move == 1)
    else:
        grid = swipe_horizontal(grid, move == 0)

    print_grid(grid)

if __name__ == '__main__':
    main()
