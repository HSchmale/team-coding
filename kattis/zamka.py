# zamka.py

l = int(input())
d = int(input())
x = int(input())

def digit_sum(x):
    return sum(int(y) for y in str(x))

for i in range(l, d + 1):
    n = i
    if digit_sum(i) == x:
        break


for i in reversed(range(l, d + 1)):
    m = i
    if digit_sum(i) == x:
        break

print(n)
print(m)
