n, b = map(int, input().split())
print("yes" if n <= 2**(b) else "no")
