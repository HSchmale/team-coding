import math
a, i = [int(x) for x in input().split()]
guess = a * i
while math.ceil(guess / a) >= i:
    guess -= 1
print(guess + 1)
