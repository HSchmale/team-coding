

class CarSpec:
    def __init__(self, line):
        line = line.split()
        self.model = line[0]
        self.p, self.q, self.k = map(int, line[1:])

def do_car_test():
    n, m = map(int, input().split())
    cars = []
    for i in range(n):
        cars.append(CarSpec(input()))
        print(cars[-1])

if __name__ == '__main__':
    num_test_case = int(input())
    for _ in range(num_test_case):
        do_car_test()
