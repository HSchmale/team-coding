import sys

variables = {}

def my_eval(args):
    try:
        args[0] = str(variables[args[0]])
        args[2] = str(variables[args[2]])
        if args[1] == '=':
            args[1] = '=='
        return 'true' if eval(' '.join(args)) else 'false'
    except KeyError as e:
        return 'undefined'
        

for line in sys.stdin.readlines():
    args = line.split()
    if args[0] == 'define':
        variables[args[2]] = int(args[1])
    elif args[0] == 'eval':
        print(my_eval(args[1:]))
