n = int(input())

university = set()
winners = 0
for i in range(n):
    uni, team = input().split()
    if uni not in university and winners < 12:
        print(uni, team)
        winners += 1
    university.add(uni) 
