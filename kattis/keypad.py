#!/usr/bin/env python

def generateMatrix(r_state, c_state):
    kp = [c_state for i in range(len(r_state))]
    for i,v in enumerate(r_state):
        if v == 0:
            kp[i] = [0 for i in range(len(c_state))]
    return kp

def testCase():
    r,c = tuple(map(int, input().split(" ")))
    matrix = [[int(c) for c in str(input())] for y in range(r)]
    r_state = [0 for i in range(r)]
    c_state = [0 for i in range(c)]
    for cr,ir in enumerate(matrix):
        for cc,jc in enumerate(ir):
            if jc == 1:
                c_state[cc] = 1
                r_state[cr] = 1
    gkp = generateMatrix(r_state, c_state)
    if gkp == matrix:
        print(True)
    else:
        print("impossible")
    print("----------")

test_cases = int(input())
for i in range(test_cases):
    testCase()
