#!/usr/bin/python

ints = map(int, input().split())
letters = input()
mydict = {}
for l,i in zip('ABC', sorted(ints)):
    mydict[l] = i
x = [0,0,0]
for i, l in enumerate(letters):
    x[i] = mydict[l]
print(' '.join(map(str, x)))
