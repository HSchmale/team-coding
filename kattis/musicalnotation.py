n = int(input())
notes = input().split()

sheet = []
for i, note in enumerate(range(ord('A'), ord('Z') + 1)):
    sheet.append(chr(note) + ": ")
for i, note in enumerate(range(ord('a'), ord('z') + 1)):
    sheet.append(chr(note) + ": ")

print(sheet)
