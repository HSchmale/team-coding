from math import log10,ceil,log
import sys

def sier_circum(n):
    return 3*(3/2)**n

dx = log(3/2) / log(10) 
for i,line in enumerate(sys.stdin.readlines()):
    n = int(line)
    b = int(dx * (n+1)) + 1
    print("Case {}: {}".format(i + 1, b))
