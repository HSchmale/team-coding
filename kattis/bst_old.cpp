#include <iostream>
#include <cassert>
#include <map>

using std::cout;
using std::cin;
using std::endl;
using std::cerr;

class TreeNode {
public:
    int         value;
    size_t      depth;
    TreeNode*   left;
    TreeNode*   parent;
    TreeNode*   right;

    TreeNode(int n, size_t d = 0, TreeNode* p = nullptr)
    : value(n), depth(d), left(nullptr), parent(p), right(nullptr) {}
    
    TreeNode()
    : TreeNode(0) {}
};


class Bst {
private:
    TreeNode header;
    std::map<int,TreeNode*> cache;
    
    TreeNode* 
    insert(int v, TreeNode*& root, size_t& depth, TreeNode* parent) {
        if(root == nullptr) {
            root = new TreeNode(v, depth, parent);
            return root;
        } 
        
        depth++;
        if(root->value < v) {
            return insert(v, root->left, depth, root);
        } else {
            return insert(v, root->right, depth, root);
        }
    }

public:
    Bst() {}

    void insert(int v, size_t& depth) {
        TreeNode* node;
        if (header.right != nullptr && v < header.right->value) {
            depth = header.right->depth;
            node = insert(v, header.right, depth, header.right->parent);
            header.right = node;
        } else if (header.left != nullptr && v > header.left->value) {
            depth = header.left->depth;
            node = insert(v, header.left, depth, header.left->parent); 
            header.left = node;
        } else {
            node = insert(v, header.parent, depth, &header);
            if (header.right == nullptr || v < header.right->value) 
                header.right = node;
            if (header.left == nullptr || v > header.left->value)
                header.left = node; 
        }
    };
};


int 
main() {
    size_t counter = 0;
    size_t n = 0, depth = 0, value = 0;
    Bst tree;

    cin >> n;
    for (size_t i = 0; i < n; ++i) {
        cin >> value;
        tree.insert(value, depth);
        counter += depth;
        depth = 0;
        cout << counter << "\n";
    }
    return 0; 
}
