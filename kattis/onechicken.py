N, M = map(int, input().split())
pieces = 'pieces'
if abs(N - M) == 1:
    pieces = 'piece'
d = N - M
if d < 0:
    print("Dr. Chaz will have {} {} of chicken left over!".format(abs(d), pieces))
else:
    print("Dr. Chaz needs {} more {} of chicken!".format(abs(d), pieces))
