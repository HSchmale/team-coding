import sys

probs = {}
for line in map(str.strip, sys.stdin.readlines()):
    if line == "-1":
        break
    time, p, stat = line.split()
    x = (time, stat == 'right')
    try:
        probs[p].append(x)
    except KeyError:
        probs[p] = [x]

tot_pens = 0
tot_time = 0
probsolved = 0
for k, v in probs.items():
    for i, (time, status) in enumerate(v):
        if status:
            tot_pens += i
            tot_time += int(time)
            probsolved += 1
            break

print(probsolved, tot_pens * 20 + tot_time) 
