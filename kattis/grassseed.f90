program grassseed
    implicit none
    integer         :: lawn_count, i
    real            :: cost_per_foot, x, y, total_area, cost

    read*, cost_per_foot
    read*, lawn_count
    do 10 i = 1, lawn_count
        read (*,*) x, y
        total_area = total_area + x * y
    10 continue
    cost = total_area * cost_per_foot
    print*, cost 
end program grassseed
