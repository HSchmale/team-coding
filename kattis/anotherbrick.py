import sys

h, w, n = map(int, input().split())
layer = 0
fill_layer = 0
for brick in map(int, input().split()):
    if fill_layer + brick > w:
        break
    elif fill_layer + brick == w:
        fill_layer = 0
        layer += 1
        if layer > h:
            print("YES")
            sys.exit(0)
    else:
        fill_layer += brick

if layer < h and fill_layer < w:
    print("NO")
else:
    print("YES")
