from collections import Counter

alphabet = {chr(x) for x in range(ord('a'), ord('z') + 1)}

n = int(input())
for i in range(n):
    s = input().lower()
    x = Counter(c for c in s if c in alphabet)
    diff = alphabet - set(x.keys())
    
    if len(diff) == 0:
        print('pangram')
    else:
        print('missing', ''.join(sorted(diff)))
