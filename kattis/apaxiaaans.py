def remove_dups(x):
    s = x[0]
    for c in x:
        if c != s[-1]:
            s += c
    return s

print(remove_dups(input()))
