# darts.py

from math import sqrt

scores = [(k**2,v) for k,v in zip(range(20,220,20), range(10,0,-1))]

for t in range(int(input())):
    s = 0
    for i in range(int(input())):
        x, y = map(int, input().split())
        dis = x**2 + y**2
        for k, v in scores:
            if dis < k:
                s += v
                print(x,y,v)
                break
    print(s)
