n, T = map(int, input().split())
tasks = map(int, input().split())

task_count = 0
time = 0
for task in tasks:
    time += task
    if time > T:
        break
    task_count += 1
print(task_count) 
