C = int(input())
for i in range(C):
    vals = [int(x) for x in input().split()]
    avg = sum(vals[1:]) / vals[0]
    above = len([x for x in vals if x > avg])
    print("%.3f%%" % (above / (vals[0]) * 100))
