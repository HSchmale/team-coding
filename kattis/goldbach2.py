import math

N = 32000
primes = [True for i in range(N)]
primes[0] = False
primes[1] = False

for i in range(2, int(math.sqrt(N))):
    if primes[i]:
        for j in range(i*i, N, i):
            primes[j] = False
primes = {k for k,v in enumerate(primes) if v == True}

cases = int(input())
for i in range(cases):
    val = int(input())
    reduced = {i for i in primes if i < val}
    solutions = set()
    for i in reduced:
        if val - i in reduced:
            solutions.add((min(i, val - i), max(i, val - i)))
    print("{} has {} representation(s)".format(val, len(solutions)))
    for i in sorted(solutions):
        print('{}+{}'.format(*i))
    print()
