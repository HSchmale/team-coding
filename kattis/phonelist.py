def load_n_lines(n):
    return [input() for x in range(n)]

TRIE_END = 'TRIE_END_'

def make_trie(word, root=None):
    current_dict = root
    for letter in word:
        current_dict = current_dict.setdefault(letter, {})
        if TRIE_END in current_dict:
            raise ValueError
    current_dict[TRIE_END] = TRIE_END
    return root

def check_const_list():
    lines = int(input())
    nums = load_n_lines(lines)
    root = dict()
    for word in nums:
        current_dict = root
        for letter in word:
            current_dict = current_dict.setdefault(letter, {})
            if TRIE_END in current_dict:
                print("NO")
                return
        current_dict[TRIE_END] = TRIE_END
    print("YES")

def main():
    cases = int(input())
    for i in range(cases):
        check_const_list()

if __name__ == '__main__':
    main() 
