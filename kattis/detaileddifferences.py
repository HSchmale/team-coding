def tc():
    x = input()
    y = input()
    s = ""
    for a, b in zip(x, y):
        s += "*" if a != b else "."
    print(x)
    print(y)
    print(s)
    print()

t = int(input())
for i in range(t):
    tc()

