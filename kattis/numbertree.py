inp = input().split()
if len(inp) == 1:
    h, path = int(inp[0]), ''
else:
    h, path = int(inp[0]), inp[1]

max_value = 2**(h + 1) - 1

current_index = 0
for c in path:
    if c == 'L':
        current_index = 2 * current_index + 1
    else:
        current_index = 2 * current_index + 2

print(max_value - current_index)
