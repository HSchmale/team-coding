#!/usr/bin/python3

from collections import OrderedDict
from itertools import chain

doors = OrderedDict()
rooms = {}
numbers = set()

def range_intersect(a, b):
    new = (max(a[0], b[0]), min(a[1], b[1]))
    if new[0] > new[1]:
        raise ValueError
    return new 

def DFS_Visit(v, t, rng=None): 
    if v == t:
        return set(range(rng[0], rng[1] + 1))
    paths = []
    rooms[v] = 1
    for n_v,n_rng in doors[v].items():
        try:
            if rooms[n_v] != 2:
                paths.append(DFS_Visit(n_v, t, range_intersect(rng, n_rng)))
        except:
            continue
    rooms[v] = 2
    return set(chain.from_iterable(paths)) 

def path_find(s):
    paths = []
    for v,rng in doors[s].items():
        paths.append(DFS_Visit(v, D, rng))
    return len(set(chain.from_iterable(paths)))

def main():
    global D
    N, L, B = tuple(map(int, input().split()))
    S, D = tuple(map(int, input().split()))

    for i in range(L):
        (a, b, l, u) = tuple(map(int, input().split()))
        if a not in doors:
            doors[a] = {b: (l, u)}
        else: 
            doors[a].update({b: (l, u)})
        rooms[a] = 0
        rooms[b] = 0
    rooms[S] = 1
    print(path_find(S))

if __name__ == '__main__':
    main()
