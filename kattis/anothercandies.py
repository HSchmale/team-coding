#!/usr/bin/env python2.7

import sys

def doTest():
    childs = int(sys.stdin.next().strip())
    candies = 0
    for child in range(childs):
        candy = int(sys.stdin.next().strip())
        candies += candy
    return candies % childs

tests = int(raw_input())
for i in range(tests):
    sys.stdin.next()
    res = doTest()
    print("YES" if res == 0 else "NO")
