#include <cstdio>
#include <string>
#include <map>
#include <set>

using std::string;

typedef std::set<char> CharSet;
typedef CharSet::iterator CharSetIt;
typedef std::map<char,CharSet> LetterTr;

bool attemptTR (LetterTr& trs, string word, string tr) {
    if (word.length () != tr.length())
        return false;
    for (int i = 0; i < word.length (); ++i) {
        if (word[i] != tr[i]) {
            CharSet& set = trs[word[i]];
            CharSetIt it = set.find (tr[i]);
            if (it != set.end()) {
                continue;
            }
            return false;
        }
    } 
    
    return true;
}

void expandTrMap (LetterTr& trs) {
    for(auto& p : trs) {
        CharSet& parent = p.second;
        for(char c : p.second) {
            LetterTr::iterator it = trs.find(c);
            if (it != trs.end()) {
            } 
        }
    }
}

int main () {
    int m, n;
    LetterTr translations;
   
    scanf ("%d %d\n", &m, &n);
    
    for (int i = 0; i < m; ++i) {
        char a, b;
        scanf ("%c %c\n", &a, &b);
        translations[a].insert (b);
    }

    expandTrMap (translations);

    for (int i = 0; i < n; ++i) {
        char a[70], b[70];
        scanf ("%s %s\n", a, b);
        if (attemptTR (translations, a, b)) {
            puts ("yes");
        } else {
            puts ("no");
        }
    }

    return 0; 
}
