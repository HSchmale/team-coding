n = int(input())
p = []
for i in range(n):
    x = input().strip()
    p.append((int(x[:-1]), int(x[-1:])))
print(sum(map(lambda x: pow(*x), p)))
