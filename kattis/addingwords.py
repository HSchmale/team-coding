import sys

varmap = {}
valmap = {}

while True:
    line = sys.stdin.readline().split()
    if not line: break
    if line[0] == 'def':
        if line[1] in varmap:
            del valmap[varmap[line[1]]]
        varmap[line[1]] = int(line[-1])
        valmap[int(line[-1])] = line[1]
    elif line[0] == 'calc':
        try:
            adding = True
            result = 0
            for index, value in enumerate(line[1:-1]):
                if index % 2 == 0:
                    # adding
                    if adding:
                        result += varmap[value]
                    else:
                        result -= varmap[value]
                else:
                    adding = True if value == '+' else False
            line.append(valmap[result])
        except KeyError:
            line.append('unknown')
        print(' '.join(line[1:]))
    elif line[0] == 'clear':
        varmap = {}
        valmap = {}


