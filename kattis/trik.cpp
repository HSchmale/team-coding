#include <iostream>
#include <string>

using namespace std;


int main() {
    string s;
    cin >> s;
    bool b[] = {1, 0, 0};
    for(auto c : s) {
        switch(c) {
        case 'A':
            swap(b[0], b[1]);
            break;
        case 'B':
            swap(b[1], b[2]);
            break;
        case 'C':
            swap(b[0], b[2]);
            break;
        }
    }
    for(int i = 0; i < 3; ++i)
        if(b[i] == 1) {
            cout << i + 1 << endl;
            break;
        }
    return 0; 
}
