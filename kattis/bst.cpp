#include <iostream>
#include <cassert>
#include <map>

using std::cout;
using std::cin;
using std::endl;
using std::cerr;


int 
main() {
    size_t counter = 0;
    size_t n = 0, depth = 0, value = 0;

    cin >> n;

    std::map<int,size_t> bst;
    cin >> value;
    
    bst[value] = 0;
    cout << "0\n"; 

    for (size_t i = 1; i < n; ++i) {
        cin >> value;

        auto it = bst.lower_bound(value);
        depth = 0;
        if (it != bst.end())
            depth = it->second + 1;
        if (it != bst.begin()) {
            --it;
            depth = std::max(depth, it->second + 1);
        }
        bst[value] = depth;

        counter += depth;
        cout << counter << "\n";
    }
    return 0; 
}
