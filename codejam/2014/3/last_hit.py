def load_test_case():
    P, Q, N = map(int, input().split())
    monsters = []
    for i in range(N):
        h, g = map(int, input().split())
        monsters.append((h, g))
    return P, Q, monsters 

T = int(input())
for i in range(T):
    load_test_case()
