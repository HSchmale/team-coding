#!/usr/bin/env python
# 398

while True:
    vals = [float(x) for x in input().split()]
    if vals[0] < 0 and vals[1] < 0:
        break
    print("{:0.2f}".format(round(vals[0] * vals[1], 2)))

      
