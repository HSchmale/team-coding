#!/usr/bin/python
# 398

import sys
from collections import OrderedDict

sys.setrecursionlimit(1000000)

def dist(i1, i2):
    return (i1[0] - i2[0]) ** 2 + (i1[1] - i2[1]) ** 2 

def find_best(invaders, R):
    def test(arr, i):
        if len(arr) == 0:
            return 1
        max_cnt = 1
        for idx, val in enumerate(arr):
            if dist(val, i) < R:
                arr2 = arr[:]
                del arr2[idx]
                cnt = 1 + test(arr2, val)
                if cnt > max_cnt:
                    (max_cnt, max_val) = (cnt, val)
        return max_cnt
    R = R * R + 0.00001
    chances = OrderedDict()
    for idx, val in enumerate(invaders):
        arr = invaders[:]
        del arr[idx]
        x = test(arr, val)
        if x not in chances:
            chances[x] = val
        elif chances[x] > val:
            chances[x] = val
    maximum = max(chances, key=chances.get)
    return (chances[maximum][0], chances[maximum][1], maximum)

def test_case():
    (N, R) = tuple([int(x) for x in input().split()])
    if N == 0 and R == 0:
        return False
    invaders = []
    for i in range(N):
        invaders.append([int(x) for x in input().split()])
    (x, y, killed) = find_best(invaders, R)
    print("{} {} {}".format(killed, x, y)) 

while True:
    if test_case() == False:
        break
    try:
        pass
    except Exception:
        break
