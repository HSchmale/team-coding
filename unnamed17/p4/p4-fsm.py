#!/usr/bin/env python
# 398

class Fsm:
    def __init__(self):
        self.states = {}
        self.acceptedStates = []
        self.state = 1
    
    def add_transition(self, stateNum, string, target):
        stateNum = int(stateNum)
        target = int(target)
        if stateNum not in self.states:
            self.states[stateNum] = {}
        for s in string:
            self.states[stateNum][s] = target 
 
    def reset(self):
        self.state = 1 

    def validate(self, string):
        for s in string:
            if s in self.states[self.state]:
                self.state = self.states[self.state][s]
            else: return False
        return self.state in self.acceptedStates
    
test_cases = int(input())
for i in range(test_cases):
    fsm = Fsm()
    states = int(input())
    for j in range(states):
        x = input().split()
        fsm.add_transition(x[0], x[1], x[2])
    fsm.acceptedStates = [int(x) for x in input().split()]
    num_str = int(input())
    for i in range(num_str):
        x = input()
        print("yes" if fsm.validate(x) else "no")
        fsm.reset()
    print()

